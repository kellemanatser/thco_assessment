
interface PaginationOptions {
    page: number;
    limit: number;
  }
  
  export function paginateQuery(data: any[], options: PaginationOptions) {
    const { page, limit } = options;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
  
    const results = data.slice(startIndex, endIndex);
  
    return {
      results,
      previous: page > 1 ? page - 1 : null,
      next: endIndex < data.length ? page + 1 : null,
    };
  }
  