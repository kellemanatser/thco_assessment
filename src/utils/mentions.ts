export function extractUserIdsFromText(text: string): string[] {
    const regex = /@([a-zA-Z0-9_]+)/g; // Regular expression to match mentions
    const matches = text.match(regex); // Find all mentions in the text
    
    if (!matches) {
      return []; // Return an empty array if no mentions found
    }
    
    // Extract user IDs from the matches (remove '@' symbol)
    const userIds = matches.map(match => match.substring(1));
  
    return userIds;
  }
  