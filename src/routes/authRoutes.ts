import express from 'express';
import {register, login} from '../controllers/authControllers';

const router = express.Router();

// Routes related to authentication

// Register a new user
router.post('/auth/register', register);

// Login user
router.post('/auth/login', login);

export default router;
