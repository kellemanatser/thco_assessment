import express, { Request, Response } from 'express';
import { createPost, getAllPosts, getPostById, likePost, commentOnPost } from '../controllers/postControllers';

import { authenticate } from '../middlewares/authentication';

const router = express.Router();




// Route to create a new post
router.post('/create-post', authenticate, createPost);

// Route to get all posts
router.get('/get-all-posts', getAllPosts);

// Route to get a specific post by ID
router.get('/get-post/:postId', getPostById);

// Route to like a post
router.post('/like-post', authenticate, likePost);

// Route to comment on a post
router.post('/comment', authenticate, commentOnPost);

export default router;








