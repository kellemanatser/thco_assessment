import express from 'express';
import { authenticate } from '../middlewares/authentication';
import { followUser, unfollowUser, getFollowers, getFollowing } from '../controllers/followControllers';

const router = express.Router();

// Route to follow a user
router.post('/follow-user', authenticate, followUser);
router.post('/unfollow-user', authenticate, unfollowUser);

// Route to get followers of a user
router.get('/get-followers/:userId', authenticate, getFollowers);

// Route to get users followed by a user
router.get('/get-following/:userId', authenticate, getFollowing);

export default router;
