import express from 'express';
import UserController from '../controllers/userControllers';

const router = express.Router();

// Routes related to user management

// Get user profile
router.get('/:userId', UserController.getUserProfile);

export default router;
