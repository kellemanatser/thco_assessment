import mongoose, { Schema, Document } from 'mongoose';

// Define a new interface for mentions
export interface Mention {
    userId: string;
}

// Update the Comment interface to include mentions
export interface Comment {
    userId: string;
    text: string;
    createdAt: Date;
    mentions: Mention[]; // Add mentions to comments
}

// Update the Post interface to include mentions
export interface Post extends Document {
    userId: string;
    text: string;
    imageUrl?: string;
    videoUrl?: string;
    likes: string[]; 
    comments: Comment[];
    createdAt: Date;
    updatedAt: Date;
    likesCount: number;
    commentsCount: number;
    mentions: Mention[]; // Add mentions to posts
}

const mentionSchema: Schema = new Schema({
    userId: { 
      type: Schema.Types.ObjectId, 
      ref: 'User', 
      required: true 
    }
});

const commentSchema: Schema = new Schema({
    userId: { 
      type: Schema.Types.ObjectId, 
      ref: 'User', 
      required: true 
    },
    text: { 
      type: String, 
      required: true 
    },
    createdAt: { 
      type: Date, 
      default: Date.now 
    },
    mentions: [mentionSchema] // Add mentions array to comments
});

const postSchema: Schema = new Schema({
    userId: { 
      type: Schema.Types.ObjectId, 
      ref: 'User', 
      required: true
    },
    text: { 
      type: String, 
      required: true 
    },
    imageUrl: { 
      type: String 
    },
    videoUrl: { 
      type: String 
    },
    likes: [{ 
      type: Schema.Types.ObjectId, 
      ref: 'User' 
    }], 
    comments: [commentSchema],
    createdAt: { 
      type: Date, 
      default: Date.now 
    },
    updatedAt: { 
      type: Date, 
      default: Date.now 
    },
    likesCount: { 
      type: Number, 
      default: 0 
    },
    commentsCount: { 
      type: Number, 
      default: 0 
    },
    mentions: [String] // Adjusted to store only user IDs directly in the posts
});

const PostModel = mongoose.model<Post>('Post', postSchema);

export default PostModel;

