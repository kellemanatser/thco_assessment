import mongoose, { Document, Schema } from 'mongoose';

export interface User extends Document {
  name: string;
  username: string;
  email: string;
  password: string;
  following: string[]; // Array of user IDs that the user is following
  followers: string[]; // Array of user IDs who are following the user
  followersCount: number; // Count of followers
  createdAt: Date;
}

const userSchema: Schema<User> = new Schema({
  name: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  following: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  followers: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  followersCount: {
    type: Number,
    default: 0,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const UserModel = mongoose.model<User>('User', userSchema);

export default UserModel;
