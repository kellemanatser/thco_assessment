
io.on('connection', (socket) => {
    console.log('Client connected:', socket.id);
  
    // Custom event: Handle mentions
    socket.on('mention', (data) => {
      console.log('Mention event received:', data);
      // Emit notification to other users
      socket.broadcast.emit('mention', data);
    });
  
    // Custom event: Handle likes
    socket.on('like', (data) => {
      console.log('Like event received:', data);
      // Emit notification to other users
      socket.broadcast.emit('like', data);
    });
  
    // Handle other custom events similarly
  });
  