import { Request, Response, NextFunction } from 'express';

export function errorHandler(err: Error, req: Request, res: Response, next: NextFunction) {
  console.error('Error:', err);

  if (res.headersSent) {
    return next(err);
  }

  return res.status(500).json({ message: 'Internal server error' });
}
