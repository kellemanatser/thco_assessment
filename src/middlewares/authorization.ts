import { Request, Response, NextFunction } from 'express';

// Define the user payload interface
interface UserPayload {
    userId: string;
    role:string
    // Add any other user properties here
}

// Augment the express request interface
declare module 'express' {
    interface Request {
        user?: UserPayload;
    }
}

export function authorize(allowedRoles: string[]) {
    return (req: Request, res: Response, next: NextFunction) => {
        // Check if user's role is included in the allowedRoles array
        if (!req.user) {
            return res.status(401).json({ message: 'Unauthorized: User not authenticated' });
        }

        if (!allowedRoles.includes(req.user.role)) {
            return res.status(403).json({ message: 'Forbidden: Insufficient permissions' });
        }

        // Move to the next middleware
        next();
    };
}
