import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

// Define the user payload interface
interface UserPayload {
    role: string;
    userId: string;
    // Add any other user properties here
  }
  
  // Augment the express request interface
  declare module 'express' {
    interface Request {
      user?: UserPayload;
    }
  }

  export function authenticate(req: Request, res: Response, next: NextFunction) {
    // Get the token from the request headers
    const token = req.headers.authorization?.split(' ')[1];
  
    if (!token) {
      return res.status(401).json({ message: 'Unauthorized: Missing token' });
    }
  
    try {
      // Verify the token
      const decoded = jwt.verify(token, process.env.JWT_SECRET || '') as UserPayload;
  
      // Attach the decoded token payload to the request object
      req.user = decoded;
  
      // Move to the next middleware
      next();
    } catch (error) {
      return res.status(401).json({ message: 'Unauthorized: Invalid token' });
    }
  }