import express from 'express';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import http from 'http';
import mongoose from 'mongoose'; 
import { Server as SocketIOServer } from 'socket.io'; // Import SocketIOServer here

import authRoutes from './routes/authRoutes';
import userRoutes from './routes/userRoutes';
import postRoutes from './routes/postRoutes';
import followRoutes from './routes/followRoutes';

import {errorHandler} from './middlewares/errorHandling'
import { authenticate } from './middlewares/authentication';

dotenv.config();

const app = express();

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'));

// MongoDB connection configuration
const mongodbUri = process.env.MONGODB_URI;
if (!mongodbUri) {
  console.error('MongoDB URI is not defined');
  process.exit(1);
}

mongoose.connect(mongodbUri, {
//   useNewUrlParser: true,
//   useUnifiedTopology: true,
}).then(() => {
  console.log('Connected to MongoDB');
}).catch((error) => {
  console.error('Error connecting to MongoDB:', error);
  process.exit(1);
});

// Routes
app.use('/api/v1', [authRoutes, postRoutes, userRoutes, followRoutes]);

// Error handler middleware
app.use(errorHandler);

// Create the HTTP server
const server = http.createServer(app);

// Create the Socket.IO server and handle WebSocket connection event
const io = new SocketIOServer(server);
io.on('connection', (socket) => {
    console.log('Client connected:', socket.id);
  
    // Handle custom events here
});

// Start the server
const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
