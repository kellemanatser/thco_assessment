import { Request, Response } from 'express';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import UserModel from '../models/userModel';


export async function register(req: Request, res: Response) {
  try {
    const { name, username, email, password } = req.body;

    // Check if user already exists
    const existingUser = await UserModel.findOne({ email });
    if (existingUser) {
      return res.status(400).json({ message: 'User already exists' });
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create new user
    const newUser = await UserModel.create({
      name,
      username,
      email,
      password: hashedPassword,
    });

    // Construct the user object with desired properties
    const user = {
      name: newUser.name,
      username: newUser.username,
      email: newUser.email,
      userId: newUser._id, // Assuming the user ID is stored in the _id field
    };

    res.status(201).json({ message: 'User registered successfully', user });
  } catch (error) {
    console.error('Error registering user:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
}


  export async function login(req: Request, res: Response) {
    try {
      const { username, password } = req.body;

      // Find user by email
      const user = await UserModel.findOne({ username });
      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      // Check password
      const isPasswordValid = await bcrypt.compare(password, user.password);
      if (!isPasswordValid) {
        return res.status(401).json({ message: 'Invalid password' });
      }

      // Generate JWT token
      const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET || '', { expiresIn: '1h' });

      res.status(200).json({ message: 'Login successful', token });
    } catch (error) {
      console.error('Error logging in user:', error);
      res.status(500).json({ message: 'Internal server error' });
    }
  };

