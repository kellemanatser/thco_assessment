import { Request, Response } from 'express';
import PostModel, { Comment, Post, Mention } from '../models/postModel';
import UserModel, { User } from '../models/userModel';
import { paginateQuery } from '../utils/pagination';
import { extractUserIdsFromText } from '../utils/mentions';

export async function createPost(req: Request, res: Response) {
    try {
      const { text, imageUrl, videoUrl } = req.body;
      const userId = req.user?.userId;

      const mentionedUserIds = extractUserIdsFromText(text);

      // Fetch mentioned users from the database
      const mentionedUsers: User[] = await UserModel.find({ _id: { $in: mentionedUserIds } }, 'username');
      
      // Replace user IDs in the mentions array with usernames
      const mentionedUsernames = mentionedUsers.map(user => user.username);
      
      const newPost = new PostModel({
        userId,
        text,
        imageUrl,
        videoUrl,
        likes: [], 
        mentions: mentionedUsernames,
        likesCount: 0,
        commentsCount: 0
      });

      await newPost.save();

      res.status(201).json({ message: 'Post created successfully', post: newPost });
    } catch (error) {
      console.error('Error creating post:', error);
      res.status(500).json({ message: 'Internal server error' });
    }
  }

export async function getAllPosts(req: Request, res: Response) {
    try {
      const { page = 1, limit = 10 } = req.query; // Default to page 1 and limit 10 if not provided
  
      // Convert page and limit to numbers
      const pageNumber = parseInt(page as string, 10);
      const limitNumber = parseInt(limit as string, 10);
  
      // Fetch total count of posts (for pagination)
      const totalCount = await PostModel.countDocuments();
  
      // Fetch paginated posts
      const posts = await PostModel.find()
        .skip((pageNumber - 1) * limitNumber) // Skip records based on page number
        .limit(limitNumber) // Limit number of records per page
        .lean() // Convert documents to plain JavaScript objects
        .exec();
  
      // Paginate the fetched posts
      const paginatedPosts = paginateQuery(posts, { page: pageNumber, limit: limitNumber });
  
      // Include total count in the response
      const response = {
        totalCount,
        ...paginatedPosts,
      };
  
      res.status(200).json(response);
    } catch (error) {
      console.error('Error fetching posts:', error);
      res.status(500).json({ message: 'Internal server error' });
    }
  }

// get post by postId
export async function getPostById(req: Request, res: Response) {
  try {
      const postId = req.params.postId;
      const post = await PostModel.findById(postId).populate('userId', 'username'); // Assuming userId is a reference to the User model
      if (!post) {
          return res.status(404).json({ message: 'Post not found' });
      }
      res.status(200).json(post);
  } catch (error) {
      console.error('Error fetching post:', error);
      res.status(500).json({ message: 'Internal server error' });
  }
}


// like post logic
export async function likePost(req: Request, res: Response) {
  try {
      const { postId } = req.body;
      const userId = req.user?.userId;

      // Check if user is authenticated
      if (!userId) {
          return res.status(401).json({ message: 'Unauthorized: User not authenticated' });
      }

      const post = await PostModel.findById(postId);
      if (!post) {
          return res.status(404).json({ message: 'Post not found' });
      }

      // Check if the user already liked the post
      if (post.likes.includes(userId)) {
          return res.status(400).json({ message: 'Post already liked' });
      }

      // Add user ID to the likes array
      post.likes.push(userId);
      
      // Update likesCount field
      post.likesCount = post.likes.length;

      await post.save();

      res.status(200).json({ message: 'Post liked successfully', post: postId });
  } catch (error) {
      console.error('Error liking post:', error);
      res.status(500).json({ message: 'Internal server error' });
  }
}


// logic to comment on a posts 
export async function commentOnPost(req: Request, res: Response) {
    try {
        const { postId, text } = req.body;
        const userId = req.user?.userId;

        if (!userId) {
            return res.status(401).json({ message: 'Unauthorized: User not authenticated' });
        }

        // Extract user IDs from the comment text
        const mentionedUserIds = extractUserIdsFromText(text);

        // Initialize an empty array for mentioned users
        let mentionedUsers: User[] = [];

        // If there are mentioned user IDs, fetch them from the database
        if (mentionedUserIds.length > 0) {
            mentionedUsers = await UserModel.find({ _id: { $in: mentionedUserIds } });
        }

        // Create a new comment object
        const newComment: Comment = {
            userId: userId,
            text,
            createdAt: new Date(),
            mentions: mentionedUsers.map(user => ({ userId: user._id })),
        };

        // Find the post by ID
        const post = await PostModel.findById(postId);
        if (!post) {
            return res.status(404).json({ message: 'Post not found' });
        }

        // Push the new comment to the post comments array
        post.comments.push(newComment);
        post.commentsCount += 1; // Increment comments count

        // Save the updated post to the database
        await post.save();

        res.status(200).json({ message: 'Comment added successfully', post });
    } catch (error) {
        console.error('Error commenting on post:', error);
        res.status(500).json({ message: 'Internal server error' });
    }
}







