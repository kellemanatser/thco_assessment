// Import necessary modules and models
import { Request, Response } from 'express';
import mongoose from 'mongoose'
import UserModel from '../models/userModel';

// Controller method to follow a user
export async function followUser(req: Request, res: Response) {
    try {
        const { userId } = req.body; 

        // Check if the user is authenticated
        if (!req.user || !req.user.userId) {
            return res.status(401).json({ message: 'Unauthorized: User not authenticated' });
        }

        const currentUser = req.user.userId;

        // Check if the user is already following the target user
        const user = await UserModel.findById(userId);
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        if (user.followers.includes(currentUser)) {
            return res.status(400).json({ message: 'User is already being followed' });
        }

        // Add the current user to the followers of the target user
        user.followers.push(currentUser);
        user.followersCount += 1; // Increment followersCount
        await user.save();

        res.status(200).json({ message: 'User followed successfully' });
    } catch (error) {
        console.error('Error following user:', error);
        res.status(500).json({ message: 'Internal server error' });
    }
}

  export async function unfollowUser(req: Request, res: Response) {
    try {
        const { userId } = req.body; 

        // Check if the user is authenticated
        if (!req.user || !req.user.userId) {
            return res.status(401).json({ message: 'Unauthorized: User not authenticated' });
        }

        const currentUser = req.user.userId;

        // Find the target user
        const user = await UserModel.findById(userId);
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        // Remove the current user from the followers of the target user
        const followerIndex = user.followers.indexOf(currentUser);
        if (followerIndex !== -1) {
            user.followers.splice(followerIndex, 1);
            user.followersCount -= 1; // Decrement followersCount
            await user.save();
            res.status(200).json({ message: 'User unfollowed successfully' });
        } else {
            res.status(400).json({ message: 'User is not being followed' });
        }
    } catch (error) {
        console.error('Error unfollowing user:', error);
        res.status(500).json({ message: 'Internal server error' });
    }
}
  
// Controller method to get followers of a user
export async function getFollowers(req: Request, res: Response) {
    try {
        const { userId } = req.params; // Assuming userId is a route parameter
        
        // Find the user by userId
        const user = await UserModel.findById(userId);
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }
        
        // Fetch followers of the user, projecting only the required fields
        const followers = await UserModel.find({ _id: { $in: user.followers } })
            .select('name username'); // Select only name and username
        
        // Respond with followers and followersCount
        res.status(200).json({ followers, followersCount: user.followersCount });
    } catch (error) {
        console.error('Error fetching followers:', error);
        res.status(500).json({ message: 'Internal server error' });
    }
}
  

// Controller method to get users followed by a user
export async function getFollowing(req: Request, res: Response) {
    try {
      const { userId } = req.params;
  
      // Find the user by userId
      const user = await UserModel.findById(userId);
      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }
  
      // Fetch users followed by the user
      const following = await UserModel.find({ _id: { $in: user.following } }, '_id name username');
      res.status(200).json({ following });
    } catch (error) {
      console.error('Error fetching following users:', error);
      res.status(500).json({ message: 'Internal server error' });
    }
  }