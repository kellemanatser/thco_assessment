import { Request, Response } from 'express';
import UserModel from '../models/userModel';

const UserController = {
  async getUserProfile(req: Request, res: Response) {
    try {
      const userId = req.params.userId;

      const user = await UserModel.findById(userId);
      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      res.status(200).json(user);
    } catch (error) {
      console.error('Error fetching user profile:', error);
      res.status(500).json({ message: 'Internal server error' });
    }
  },
};

export default UserController;
