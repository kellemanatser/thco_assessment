# SOCIAL MEDIA APIS

- To run this application, navigate int social-media-apis folder:
 -npm install: to install all dependencies
 - npm run dev: to start the server on development

Local Base url: http://localhost:5000/api/v1


 - APPLICATION ENDPOINTS

Authentication Endpoints

-Register
Endpoint: POST /register
Description: Register a new user.

Example:
url: http:localhost:5000/api/v1/auth/register
{
  "name": "Jone Doe",
  "username": "johndoe"
  "email": "johndoe@gmail.com",
  "password": "securepassword"
}

-Login
Endpoint: POST /login
Description: Log in an existing user.

Example:
url:http://localhost:5000/api/v1/auth/login
{
  "username": "johndoe,
  "password": "securepassword"
}


Post Endpoints

- Create Post

Endpoint: POST /create-post
Description: Create a new post to display in the field, you can mention someone in the post by @userId

Example:
url:http://localhost:5000/api/v1/create-post
{
    "userId": "4555455454545544",  //You can get user userId from register 
    "text": "This is my post, @676464564647656",
    "imageUri": "https://miro.medium.com/image.png",
    "videoUri": "https://uploaded-vieo_link_here"
}

-Get all post
Endpoint: GET /all-Posts
Description: Get all posts displayed, pagination is implemented here to set page and limit

Example:
url: http://localhost:5000/api/v1/get-all-posts
url: http://localhost:5000/api/v1/get-all-posts?page=1&limit=5   //To set the page and limit of the data to be retireved 


- Get Post by postId
Endpoint: GET /get-post/:postId
Description: Pass the postId as a query parameter to retrive a specific post details
Example:
url: http://localhost:5000/api/v1/get-post/9088757776784774


- Like post
Endpoint: POST /like-post
Description: This likes a specific post by providing postId and userId.
Example:
url: http://localhost:5000/api/v1/like-post
{
    "postId": "659bf87634aba32b9f1ac",
    "userId": "659b3edb464984cd4af16"
}

- Comment on post

Endpoint: POST /comment
Description: comment on a post by providing userId, postId and text. In the text field, you can mention someone by @userId

Example:
url:http://localhost:5000/api/v1/comment
{
    "postId": "659c2e49f6302d3eaa0de",
    "userId": "659b3edb464984cd4af16",
    "text": "This is my comment @474774747744774" //The @474774747744774 is to mention a particular user in a post, you can mention more than one
}


Follow Endpoints

- Follow a User
Endpoint: POST /follow-user
Description: Follow a user by providing his/her userId

Example:
url:http://localhost:5000/api/v1/follow-user
{
    "userId": "659b3edb464984cd4af1648c",
}

- Unfollow a User
Endpoint: POST /unfollow-user
Description: This request unfollows a user that you have followed before

Example:
url:http://localhost:5000/api/v1/unfollow-user
{
    "userId": "659c2e49f6302d3eaa0de"
}


- Getting a User follwers
Endpoint: GET /get-followers
Description: This request get all the users of a specified user

Example:
url:http://localhost:5000/api/v1/get-followers
{
    "userId": "659c2e49f6302d3eaa0de"
}





USING THE API

#Registration and Authentication:
- Register a new user using the /auth/register endpoint.
- Log in with the registered user using the /auth/login endpoint.

#Post Activities:
- Create a post and also mention another in a post using the /create-post endpoint.
- Get all the created post using the /get-all-post endpoint.
- Get details of a post using the /get-post/:roomId endpoint.
- Comment on a post and also mention another user in a comment using /comment endpoint
- Like a specific post using /like-post endpoint

#Follow Activities:
- Follow a user using the /follow-user endpoint.
- unfollow a user using the /unfollow-user endpoint.
- Get a users' followers using the /get-followers/userId endpoint

#Incomplete Modules
 - Users registration needs to have more fields like date of birth, profile picture, gender and other details.
 - implementing real-time notification with socket.io


#Features to be added
- using email or phone as a username for signup/signin,
- activating account with and email/sms verification or otp
- forget and reset password
- Email and password validation
- Update users account
- Deleting a post
- unlike a post
- deleting a comment
- push notification
